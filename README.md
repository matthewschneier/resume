# Resume

# Matthew Schneier
-------------------------------------------------------------------------------------------------------------------
Champaign, IL | 217-840-8431 | matthew@computeraccurate.com | [Bitbucket](https://bitbucket.org/matthewschneier/workspace/projects/)

## Work Experience
### WEB DEVELOPER | 2020 - PRESENT
- Developer several internal websites using Coldfusion
### INDEPENDENT CONTRACTOR | 2019 - 2020
- Completed programming contracts involving PHP, HTML, CSS, Vue.js, and JavaScript.
### SYSTEM ADMINISTRATOR INTERN | WOLFRAM RESEARCH, INC. | 2017 - 2019
- Developed several internal websites using Python Flask and Django frameworks, NodeJS, HTML, CSS, JavaScript, JQuery, Apache and Ngninx Web Servers, Bash scripting, PostgreSQL, MondoDB, and Redis.
  - Improved layout and functionality of a rolodex website.
    - Used Redis to keep track of logins.
    - Encrypted Redis communications with spiped and backed up logs with cron.
  - Made new password change website.
    - Rewrote older Perl codebase into a more modern Python Flask codebase.
    - Used JQuery for client side form validation for UX improvement.
  - Created website to reserve servers.
    - Used MEN stack and FullCalendar to display a calendar of server reservations.
  - Programmed website to view contents of server racks.
    - Used Python Flask and PostgreSQL to build website that allowed users to perform CRUD operations on devices in server racks.
  - Updated websites from Apache to Apache 2.
  - Used LetsEncrypt and Wolfram's CA to add https to websites.
### TUTORING 2017
  - Cisco Networking and Windows Server Tutor. One of my students was recently featured as a "success story" in a campus newsletter.
  
## Certifications
- CCNA Routing and Switching
- CCENT
- Microsoft Access
- Microsoft Excel

## Education
### SOUTHERN ILLINOIS UNIVERSITY
- GPA 4.00/4.00
- Courses:
  - SQL Programming, Database Administration, Frontend Development, Computer Security
### CONTINUING EDUCATION
- Harvard University - Professional Certificate in "Computer Science for Web Programming" (expected July 2020).
  - C, Python Django, Heroku
- Udemy: React(2019), Vue(2019), WordPress(2020), AWS(currently enrolled)
### PARKLAND COLLEGE | AAS OF NETWORK ADMIN | 2017
- GPA: 3.78/4.00
- Courses:
  - Cisco Networking, Wireless Networking, Computer Hardware, Microsoft Office, Windows Server, Linux Administration, Python Programming, Network Security, Calculus.
- Projects:
  - Configured a variety of Cisco routers and switches to accomplish specific tasks (GLBP, HSRP, VRRP, GRE Tunnel, ACLs, OSPFv3, EIGRP).
  - Installed lightweight and autonomous Cisco Access Points.
  - Setup Microsoft Active Directory and VDI.
### MONTICELLO HIGH SCHOOL | 2015
- GPA: 4.00/4.00
- Class Rank: 1/126
### AWARDS/HONORS
- Parkland Colege Board of Trustees Scholarship 2015-2017
- Dean's List 2016-2017
- Illinois State Scholar 2015-2016
- President's Award for Educational Excellence 2015
